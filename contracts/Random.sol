// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

contract Random {
    function getProofableRandom() public view returns (uint256) {
        // FIXME: Use real randomness
        return rand(block.timestamp);
    }

    function rand(uint256 seed) internal pure returns (uint256) {
        return uint256(keccak256(abi.encode(seed)));
    }

    function randExpand(uint256 seed, uint256 n)
        internal
        pure
        returns (uint256)
    {
        return uint256(keccak256(abi.encode(seed, n)));
    }

    function randrange(
        uint256 random,
        uint256 min,
        uint256 max
    ) internal pure returns (uint256) {
        return min + (random % (max - min));
    }
}
