// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "./HolidayToken.sol";
import "./GiftToken.sol";
import "./Random.sol";

contract Door is Random {
    HolidayToken holidayToken;
    GiftToken giftToken;

    mapping(address => mapping(uint8 => bool)) opened;

    uint256 public openFee = 0.1 * 10**18;

    event Opened(
        address _owner,
        uint8 _doorId,
        uint256 _holidayTokenAmount,
        bool _gift
    );

    constructor(address adressOfHolidayToken, address addressOfGiftToken) {
        holidayToken = HolidayToken(adressOfHolidayToken);
        giftToken = GiftToken(addressOfGiftToken);
    }

    function openedDay(uint8 dayOfEvent) public view returns (bool) {
        return opened[msg.sender][dayOfEvent];
    }

    function openedDays() external view returns (bool[] memory) {
        bool[] memory openedList = new bool[](24);
        for (uint8 i = 0; i < 24; i++) {
            openedList[i] = openedDay(i);
        }
        return openedList;
    }

    function openedToday() external view returns (bool) {
        uint8 dayOfEvent = _getDayOfEvent();
        return openedDay(dayOfEvent);
    }

    function openDoor(uint8 dayOfEvent) public payable returns (bool) {
        require(msg.value >= openFee, "Not enough wei provided");

        uint8 today = _getDayOfEvent();

        require(dayOfEvent == today, "Only can open the door of today");

        opened[msg.sender][dayOfEvent] = true;

        uint8 randID;

        uint256 provableRand = getProofableRandom();

        uint256 holidayTokenReward = randrange(
            randExpand(provableRand, randID),
            1,
            25
        );
        randID++;
        uint256 giftRewardProb = randrange(
            randExpand(provableRand, randID),
            1,
            100
        );
        randID++;

        bool mintGiftToken = false;
        if (giftRewardProb == 73) {
            mintGiftToken = true;
            giftToken.mintRandom(msg.sender);
        }

        holidayToken.mint(msg.sender, holidayTokenReward);

        emit Opened(msg.sender, dayOfEvent, holidayTokenReward, mintGiftToken);
    }

    function openDoorToday() external payable returns (bool) {

        uint8 dayOfEvent = _getDayOfEvent();

        require(dayOfEvent > 0, "Event has not started yet");
        require(dayOfEvent <= 24, "Event has ended");

        // FIXME: uncomment after testing
        // require(opened[msg.sender][dayOfEvent] == false, "Already opened today");

        return openDoor(dayOfEvent);
    }

    function _getDayOfEvent() internal view returns (uint8) {
        uint256 eventStart = 1638313200;
        uint8 dayOfEvent = uint8((block.timestamp - eventStart) / 86400);

        return dayOfEvent;
    }
}
