// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "@openzeppelin/contracts/token/ERC721/ERC721.sol";
import "@openzeppelin/contracts/token/ERC721/extensions/ERC721Enumerable.sol";
import "@openzeppelin/contracts/access/Ownable.sol";

import "./HolidayToken.sol";
import "./Random.sol";

contract GiftToken is ERC721, ERC721Enumerable, Ownable, Random {
    HolidayToken holidayToken;

    // a mapping from an address to whether or not it can mint
    mapping(address => bool) controllers;

    mapping(uint256 => bool) minted;

    event Bought(address indexed buyer, uint256 indexed tokenId);

    constructor(address adressOfHolidayToken) ERC721("Gift", "GIFT") {
        holidayToken = HolidayToken(adressOfHolidayToken);
    }

    function _drawGift(uint32 tokenId) internal view returns (string memory) {

        // There are no shift operators in Solidity
        uint8 background_filter_trait = uint8(tokenId / 2 ** 30);


        string memory background = "iVBORw0KGgoAAAANSUhEUgAAAgAAAAIACAYAAAD0eNT6AAADEnpUWHRSYXcgcHJvZmlsZSB0eXBlIGV4aWYAAHja7ZdbkuwmDIbfWUWWgCSExHIwl6rsIMvPj409lzM1mVTOQx4ayuAWsiT0CXd3GH/9OcMfaBwph6TmueQc0VJJhStuPF6tniPFdI534y39IA/PAsxGwSzXguf92IAcGsRb3q6ZKuT6zlAZe+H4uFC3Ifbt4I5oOxK6HMRtONRtSHh7Ttfn49pWzMXt/Rba1p97/UyDr61hSGKcNZMljImjWS64d47JkLe+Ap2Ny3pOj23o0+dwqzJi4iEkEaOvCGVdJBUXY4xSoEOnhEUxivAVKWhxiIb7cuf1+/Zd5OEOfSP/jJq+Qv3cvSMdzoTeC/KJUH7mL+Wkb/LwHunJ7Z3nnB/PH+Qmj4uHXLjxzdl9znHtrqaMLee9qXsr5x30jpWt86mMbrg0+kr26gXdcSQa6qijyA70RoUYGCcl6lRp0jjnRg0hJh5smJkb4LYAoQNG4SYLblqdJpsU6eIA3VAOAik/sdDptpzuGjkcd/KA6iEYW7Xyn3r4idKc60ARRX9yhbh4lSOiWOSIQiSogQjNnVQ9E3z3z21xFRDUM82ODdZ4LAvAfyi9FZecoAWKivk6wGR9G0CKEIEiGByVRDGTKGWKxhyMCIl0AKoInSXxASykyh1BchLJgINTAN94xuhUZeVLjBchQKjkIAY2RSpgpaSoH0uOGqoqmlQ1q6lr0ZolrxOWs+X1Rq0mlkwtm5lbsRpcPLl6dnP34rVwEbxxteA8Fi+l1AqnFZYrnq5QqPXgQ4506JEPO/woR20cmrTUtOVmzVtptXOXjnPcc7fuvfQ6aKCURho68rDho4w6UWpTZpo687Tps4RZH2ob6y/9X1CjTY1PUkvRHmqQmt0maL1OdDEDMU4E4LYIkATmxSw6pcSL3GIWC+NUKCNIXXA6LWIgmAaxTnrYvZHTIPn3cAsAwb+DXFjofkDuV25fUev1/KKTk9A6hiupUXD6ZpnDK3td35TfzuGfFH46vwy9DL0MvQy9DL0MvQz9vw1N/HbAH8fwNwKztkf71iu8AAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAC4jAAAuIwF4pT92AAAAB3RJTUUH5QwJChkh6Fu+igAAABl0RVh0Q29tbWVudABDcmVhdGVkIHdpdGggR0lNUFeBDhcAAAbaSURBVHja7dYxAQAACMOwgX/PYIKPREKvVpIJAPBKSwAABgAAMAAAgAEAAAwAAGAAAAADAAAYAADAAAAABgAAMAAAgAEAAAwAAGAAAAADAAAYAADAAAAABgAADAAAYAAAAAMAABgAAMAAAAAGAAAwAACAAQAADAAAYAAAAAMAABgAAMAAAAAGAAAwAACAAQAADAAAGAAAwAAAAAYAADAAAIABAAAMAABgAAAAAwAAGAAAwAAAAAYAADAAAIABAAAMAABgAAAAAwAAGAAAMAAAgAEAAAwAAGAAAAADAAAYAADAAAAABgAAMAAAgAEAAAwAAGAAAAADAAAYAADAAAAABgAAMAAAYAAAAAMAABgAAMAAAAAGAAAwAACAAQAADAAAYAAAAAMAABgAAMAAAAAGAAAwAACAAQAADAAAYAAAwABIAAAGAAAwAACAAQAADAAAYAAAAAMAABgAAMAAAAAGAAAwAACAAQAADAAAYAAAAAMAABgAAMAAAAAGAAAMAABgAAAAAwAAGAAAwAAAAAYAADAAAIABAAAMAABgAAAAAwAAGAAAwAAAAAYAADAAAIABAAAMAAAYAADAAAAABgAAMAAAgAEAAAwAAGAAAAADAAAYAADAAAAABgAAMAAAgAEAAAwAAGAAAAADAAAYAAAwAACAAQAADAAAYAAAAAMAABgAAMAAAAAGAAAwAACAAQAADAAAYAAAAAMAABgAAMAAAAAGAAAwAABgAAAAAwAAGAAAwAAAAAYAADAAAIABAAAMAABgAAAAAwAAGAAAwAAAAAYAADAAAIABAAAMAABgAADAAEgAAAYAADAAAIABAAAMAABgAAAAAwAAGAAAwAAAAAYAADAAAIABAAAMAABgAAAAAwAAGAAAwAAAAAYAAAwAAGAAAAADAAAYAADAAAAABgAAMAAAgAEAAAwAAGAAAAADAAAYAADAAAAABgAAMAAAgAEAAAwAABgAAMAAAAAGAAAwAACAAQAADAAAYAAAAAMAABgAAMAAAAAGAAAwAACAAQAADAAAYAAAAAMAABgAADAAAIABAAAMAABgAAAAAwAAGAAAwAAAAAYAADAAAIABAAAMAABgAAAAAwAAGAAAwAAAAAYAADAAAGAAAAADAAAYAADAAAAABgAAMAAAgAEAAAwAAGAAAAADAAAYAADAAAAABgAAMAAAgAEAAAwAAGAAAMAASAAABgAAMAAAgAEAAAwAAGAAAAADAAAYAADAAAAABgAAMAAAgAEAAAwAAGAAAAADAAAYAADAAAAABgAADAAAYAAAAAMAABgAAMAAAAAGAAAwAACAAQAADAAAYAAAAAMAABgAAMAAAAAGAAAwAACAAQAADAAAGAAAwAAAAAYAADAAAIABAAAMAABgAAAAAwAAGAAAwAAAAAYAADAAAIABAAAMAABgAAAAAwAAGAAAMAAAgAEAAAwAAGAAAAADAAAYAADAAAAABgAAMAAAgAEAAAwAAGAAAAADAAAYAADAAAAABgAAMAAAYAAAAAMAABgAAMAAAAAGAAAwAACAAQAADAAAYAAAAAMAABgAAMAAAAAGAAAwAACAAQAADAAAYAAAwABIAAAGAAAwAACAAQAADAAAYAAAAAMAABgAAMAAAAAGAAAwAACAAQAADAAAYAAAAAMAABgAAMAAAAAGAAAMAABgAAAAAwAAGAAAwAAAAAYAADAAAIABAAAMAABgAAAAAwAAGAAAwAAAAAYAADAAAIABAAAMAAAYAADAAAAABgAAMAAAgAEAAAwAAGAAAAADAAAYAADAAAAABgAAMAAAgAEAAAwAAGAAAAADAAAYAAAwAACAAQAADAAAYAAAAAMAABgAAMAAAAAGAAAwAACAAQAADAAAYAAAAAMAABgAAMAAAAAGAAAwAABgAAAAAwAAGAAAwAAAAAYAADAAAIABAAAMAABgAAAAAwAAGAAAwAAAAAYAADAAAIABAAAMAABgAADAAEgAAAYAADAAAIABAAAMAABgAAAAAwAAGAAAwAAAAAYAADAAAIABAAAMAABgAAAAAwAAGAAAwAAAAAYAAAwAAGAAAAADAAAYAADAAAAABgAAMAAAgAEAAAwAAGAAAAADAAAYAADAAAAABgAAMAAAgAEAAAwAABgAAMAAAAAGAAAwAACAAQAADAAAYAAAAAMAABgAAMAAAAAGAAAwAACAAQAADAAAYAAAAAMAABgAADAAAIABAAAMAABgAAAAAwAAGAAAwAAAAAYAADAAAIABAAAMAABgAAAAAwAAGAAAwAAAAAYAADAAAGAAAAADAAAYAADAAAAABgAAMAAAgAEAAAwAAGAAAAADAAAYAADAAAAAdxbMfAT/e95f8QAAAABJRU5ErkJggg==";
        string memory background_filter;
        if (background_filter_trait == 0) {
            // maroon
            background_filter = "1 0 0 0 .5   0 1 0 0 0   0 0 1 0 0   0 0 0 1 0";
        } else if (background_filter_trait == 1) {
            // purple
            background_filter = "1 0 0 0 .5   0 1 0 0 0   0 0 1 0 .5   0 0 0 1 0";
        } else if (background_filter_trait == 2) {
            // office green
            background_filter = "1 0 0 0 0   1 0 0 0 .5   0 0 1 0 0   0 0 0 1 0";
        } else if (background_filter_trait == 3) {
            // navy blue
            background_filter = "1 0 0 0 0   0 1 0 0 0   1 0 0 0 .5   0 0 0 1 0";
        }

        string memory svg = string(abi.encodePacked('<svg id="gift" width="100%" height="100%" version="1.1" viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><filter id="background" color-interpolation-filters="sRGB" x="0" y="0" width="100%" height="100%"><feColorMatrix type="matrix" values="',
            background_filter,
            '"/></filter><image filter="url(#background)" width="512" height="512" image-rendering="pixelated" preserveAspectRatio="xMidYMid" xlink:href="data:image/png;base64,',
            background,
            '"/></svg>'
        ));
        return string(abi.encodePacked(
            "data:image/svg+xml;base64,",
            base64(bytes(svg))
        ));
    }

    function tokenURI(uint256 tokenId) public view override returns (string memory) {

        uint32 id = uint32(tokenId);

        string memory metadata = string(abi.encodePacked(
            '{"name":"test","description":"test","image": "',
            _drawGift(id),
            '","attributes":[]}'
        ));

        return string(abi.encodePacked(
            "data:application/json;base64,",
            base64(bytes(metadata))
        ));
    }

    function buy() external returns (uint256) {
        uint256 buyerBalance = holidayToken.balanceOf(msg.sender);

        uint256 price = 1;

        require(buyerBalance >= price, "Buyer does not have enough tokens");

        holidayToken.burn(msg.sender, price);

        uint256 newItemId = _mintRandom(msg.sender);

        emit Bought(msg.sender, newItemId);

        return newItemId;
    }

    /**
     * enables an address to mint / burn
     * @param controller the address to enable
     */
    function addController(address controller) external onlyOwner {
        controllers[controller] = true;
    }

    /**
     * disables an address from minting / burning
     * @param controller the address to disbale
     */
    function removeController(address controller) external onlyOwner {
        controllers[controller] = false;
    }

    function mintRandom(address to) external {
        require(controllers[msg.sender], "Only controllers can mint");

        _mintRandom(to);
    }

    function _mintRandom(address to) internal returns (uint256) {

        uint256 provableRand = getProofableRandom();

        uint256 newItemId;

        do {
            newItemId = randrange(provableRand, 0, 2**32 - 1);
        } while (minted[newItemId] == true);

        minted[newItemId] = true;

        _mint(to, newItemId);

        return newItemId;
    }

    // The following functions are overrides required by Solidity.

    function _beforeTokenTransfer(address from, address to, uint256 tokenId)
        internal
        override(ERC721, ERC721Enumerable)
    {
        super._beforeTokenTransfer(from, to, tokenId);
    }

    function supportsInterface(bytes4 interfaceId)
        public
        view
        override(ERC721, ERC721Enumerable)
        returns (bool)
    {
        return super.supportsInterface(interfaceId);
    }

    /** BASE 64 - Written by Brech Devos */

    string internal constant TABLE = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/';

    function base64(bytes memory data) internal pure returns (string memory) {
        if (data.length == 0) return '';

        // load the table into memory
        string memory table = TABLE;

        // multiply by 4/3 rounded up
        uint256 encodedLen = 4 * ((data.length + 2) / 3);

        // add some extra buffer at the end required for the writing
        string memory result = new string(encodedLen + 32);

        assembly {
            // set the actual output length
            mstore(result, encodedLen)

            // prepare the lookup table
            let tablePtr := add(table, 1)

            // input ptr
            let dataPtr := data
            let endPtr := add(dataPtr, mload(data))

            // result ptr, jump over length
            let resultPtr := add(result, 32)

            // run over the input, 3 bytes at a time
            for {} lt(dataPtr, endPtr) {}
            {
                dataPtr := add(dataPtr, 3)

                // read 3 bytes
                let input := mload(dataPtr)

                // write 4 characters
                mstore(resultPtr, shl(248, mload(add(tablePtr, and(shr(18, input), 0x3F)))))
                resultPtr := add(resultPtr, 1)
                mstore(resultPtr, shl(248, mload(add(tablePtr, and(shr(12, input), 0x3F)))))
                resultPtr := add(resultPtr, 1)
                mstore(resultPtr, shl(248, mload(add(tablePtr, and(shr( 6, input), 0x3F)))))
                resultPtr := add(resultPtr, 1)
                mstore(resultPtr, shl(248, mload(add(tablePtr, and(        input,  0x3F)))))
                resultPtr := add(resultPtr, 1)
            }

            // padding with '='
            switch mod(mload(data), 3)
            case 1 { mstore(sub(resultPtr, 2), shl(240, 0x3d3d)) }
            case 2 { mstore(sub(resultPtr, 1), shl(248, 0x3d)) }
        }

        return result;
    }
}
