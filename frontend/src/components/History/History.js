import React, { Component } from 'react'
import { ethers } from 'ethers'
import './History.css'

import giftTokenFile from '../../GiftToken.json';
import { throws } from 'assert';

class Gift extends Component {
  componentWillMount() {
    this.loadState()
  }

  async loadState() {
    const provider = new ethers.providers.Web3Provider(window.ethereum)
    const signer = provider.getSigner()
    const giftToken = new ethers.Contract(giftTokenFile.address, giftTokenFile.abi, signer)
    const signerAddress = await signer.getAddress()

    let eventList = ""

    let filter = giftToken.filters.Bought(signerAddress, null);
    let events = await giftToken.queryFilter(filter);

    eventList = events.toString()

    this.setState({ giftBought: eventList })

  }

  constructor(props) {
    super(props)
    this.state = { giftBought: [] }
  }

  render() {
    return (
      <div className="container">
        <h1>Buy History</h1>
        {/* {this.state.giftBought.map(event => <p>{event}</p>)} */}
      </div>
    );
  }
}

export default Gift;
