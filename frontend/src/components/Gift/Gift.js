import React, { Component } from 'react'
import { ethers } from 'ethers'
import './Gift.css'

import giftTokenFile from './../../GiftToken.json';
import { throws } from 'assert';

class Gift extends Component {
  componentWillMount() {
    this.loadState()
  }

  async loadState() {
    const id = this.state.id

    const provider = new ethers.providers.Web3Provider(window.ethereum)
    const giftToken = new ethers.Contract(giftTokenFile.address, giftTokenFile.abi, provider)
    const uri = await giftToken.tokenURI(id)
    this.setState({
      id: id,
      uri: uri
    })
  }

  constructor(props) {
    super(props)
    this.state = { id: props.id, uri: '' }
  }

  render() {
    return (
      <div className="container">
        <p>Gift #{this.state.id}</p>
        <Picture uri={this.state.uri} />
      </div>
    );
  }
}

function Picture(props) {
  const uri = props.uri
  const uri_json = uri.replace('data:application/json;base64,', '')
  const base64ToString = Buffer.from(uri_json, 'base64').toString('ascii')
  // const json = JSON.parse(base64ToString)
  // FIXME: somehow it not works with json parsing..
  var image = base64ToString.substring(
    base64ToString.indexOf("data:image/svg+xml;base64,"),
    base64ToString.lastIndexOf(",") - 1
  );
  return (
      <img
        src={image}
        alt="gift"
      />
  )
}

export default Gift;
