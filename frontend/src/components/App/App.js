import React, { Component } from 'react'
import { ethers } from 'ethers'
import { Container, Row, Col } from 'react-bootstrap';
import './App.css'

import holidayTokenFile from './../../HolidayToken.json';
import giftTokenFile from './../..//GiftToken.json';
import doorFile from './../../Door.json';

import Doors from './../Doors/Doors';
import Gift from './../Gift/Gift';
import History from '../History/History';

class App extends Component {
  componentWillMount() {
    this.loadState()
  }

  async loadState() {
    const provider = new ethers.providers.Web3Provider(window.ethereum)
    const signer = await provider.getSigner()
    const signerAddress = await signer.getAddress()

    const holidayToken = new ethers.Contract(holidayTokenFile.address, holidayTokenFile.abi, provider)
    const giftToken = new ethers.Contract(giftTokenFile.address, giftTokenFile.abi, provider)
    const door = new ethers.Contract(doorFile.address, doorFile.abi, provider)

    const giftTokenBalance = await giftToken.balanceOf(signerAddress)
    let giftTokenIndices = []
    for (let i = 0; i < giftTokenBalance; i++) {
      const id = await giftToken.tokenOfOwnerByIndex(signerAddress, i)
      giftTokenIndices.push(id.toString())
    }

    this.setState({
      wallet: {
        address: signerAddress,
        balance: ethers.utils.formatEther(await signer.getBalance()).toString(),
      },
      holidayToken: {
        address: await holidayToken.address,
        balance: ethers.utils.formatUnits(await holidayToken.balanceOf(signerAddress), await holidayToken.decimals()).toString(),
      },
      giftToken: {
        address: await giftToken.address,
        balance: ethers.utils.formatUnits(giftTokenBalance, 0).toString(),
        owned: giftTokenIndices,
      }
    })
  }

  constructor(props) {
    super(props)
    this.state = { wallet: '', holidayToken: '', giftToken: { address: '', balance: '', owned: [] } };
  }

  buyGift = () => {
    console.log('Buy gift')
    const provider = new ethers.providers.Web3Provider(window.ethereum)
    const signer = provider.getSigner()
    const giftToken = new ethers.Contract(giftTokenFile.address, giftTokenFile.abi, signer)

    let result = giftToken.buy()

    let filter = giftToken.filters.Bought(result.from, null);
    giftToken.on(filter, (buyer, tokenId) => {
      this.loadState()
      console.log('Bought', tokenId)
    })
  }

  render() {
    return (
      <div className="container">
        <h1>Crypto Advent Calendar</h1>
        <p>Your account: {this.state.wallet.address}</p>
        <p>Your ETH balance: {this.state.wallet.balance}</p>
        <p>HolidayToken address: {this.state.holidayToken.address}</p>
        <p>HolidayToken balance: {this.state.holidayToken.balance}</p>
        <p>GiftToken address: {this.state.giftToken.address}</p>
        <p>GiftToken balance: {this.state.giftToken.balance}</p>
        <p/>
        <button onClick={this.buyGift}>
          Buy Gift
        </button>
        <Doors />
        <Container>
          <Row xs="1" sm="2" md="3" lg="3" xl="3" xxl="3">
            {this.state.giftToken.owned.map(id =>
            <Col>
              <Gift id={id} />
              <p></p>
            </Col>
            )}
          </Row>
        </Container>
        <History />
      </div>
    );
  }
}

export default App;
