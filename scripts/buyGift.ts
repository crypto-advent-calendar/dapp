import { ethers } from "hardhat";

async function main() {
    const GiftToken = await ethers.getContractFactory("GiftToken");
    const giftToken = await GiftToken.deployed();

    await giftToken.buy();
}

main().catch((error) => {
    console.error(error);
    process.exitCode = 1;
});
