import { FormatTypes } from "ethers/lib/utils";
import { ethers } from "hardhat";

const fs = require("fs");

async function main() {
    // Deploy HolidayToken
    const HolidayToken = await ethers.getContractFactory("HolidayToken");
    const holidayToken = await HolidayToken.deploy(100);

    await holidayToken.deployed();
    console.log("HolidayToken deployed to:", holidayToken.address);

    // Deploy GiftToken
    const GiftToken = await ethers.getContractFactory("GiftToken");
    const giftToken = await GiftToken.deploy(holidayToken.address);

    await giftToken.deployed();
    console.log(
        "GiftToken deployed to:",
        giftToken.address,
        "and linked to HolidayToken"
    );

    // Deploy Door
    const Door = await ethers.getContractFactory("Door");
    const door = await Door.deploy(holidayToken.address, giftToken.address);

    await giftToken.deployed();
    console.log(
        "Door deployed to:",
        door.address,
        "and linked to HolidayToken and GiftToken"
    );

    // Adding controller

    await holidayToken.addController(giftToken.address);
    await holidayToken.addController(door.address);
    console.log("Add GiftToken and Door as controllers to HolidayToken");

    await giftToken.addController(door.address);
    console.log("Add Door as controller to GiftToken");

    // write the contract address and abi to a file for frontend
    fs.writeFileSync(
        "./frontend/src/HolidayToken.json",
        JSON.stringify({
            address: holidayToken.address,
            abi: JSON.parse(holidayToken.interface.format(FormatTypes.json).toString()),
        })
    );
    fs.writeFileSync(
        "./frontend/src/GiftToken.json",
        JSON.stringify({
            address: giftToken.address,
            abi: JSON.parse(giftToken.interface.format(FormatTypes.json).toString()),
        })
    );
    fs.writeFileSync(
        "./frontend/src/Door.json",
        JSON.stringify({
            address: door.address,
            abi: JSON.parse(door.interface.format(FormatTypes.json).toString()),
        })
    );
}

main().catch((error) => {
    console.error(error);
    process.exitCode = 1;
});
